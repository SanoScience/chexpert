import json
import os

from easydict import EasyDict as edict
from pytorch_grad_cam.utils.model_targets import ClassifierOutputTarget
from torch.nn import BCEWithLogitsLoss
from torch.optim import Adam

from data.dataset import ImageDataset, ImageDataset_full
from metrics import ACC, AUC
from model.chexpert import CheXpert_model

from pytorch_grad_cam import GradCAM
from pytorch_grad_cam.utils.image import show_cam_on_image
from torchvision import transforms
from torch.autograd import Variable
import numpy as np
import PIL

import argparse

# Initiate the parser
parser = argparse.ArgumentParser()
parser.add_argument("--file", type=str, nargs='+', help="path to patient dir where are images to predict", required=True)

# Read arguments from the command line
args = parser.parse_args()

if not args.file:
    exit(1)

os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"

cfg_path = './config/example.json'

with open(cfg_path) as f:
    cfg = edict(json.load(f))

optimizer = Adam
loss_func = BCEWithLogitsLoss()

if cfg['full_classes']:
    data_class = ImageDataset_full
else:
    data_class = ImageDataset

metrics_dict = {'acc': ACC(), 'auc': AUC()}

chexpert_model = CheXpert_model(cfg, loss_func, metrics_dict)
if cfg['ckp_path'] != './':
    chexpert_model.load_ckp(cfg['ckp_path'])

target_layer = chexpert_model.model.get_target_layer()

cam = GradCAM(model=chexpert_model.model, target_layers=[target_layer], use_cuda=False)

for file in args.file:
    if file.endswith('.jpg'):
        print(os.path.join(file) + "\nPrediction:\n")
        predict = chexpert_model.predict_from_file(os.path.join(file))
        print(predict)
        with open('prediction.txt', 'a+') as F:
            F.write("\n" + os.path.join(file) + "\nPrediction:\n")
            F.write(str(predict))

        image_rgb = PIL.Image.open(os.path.join(file)).convert('RGB')
        convert = transforms.ToTensor()
        input_tensor = convert(image_rgb).float()
        v = Variable(input_tensor, requires_grad=True)
        v = v.unsqueeze(0)

        chexpert_model.model.cfg["heatmap_flag"] = True
        grayscale_cam = cam(input_tensor=v)
        img_np = np.array(image_rgb) / 255
        chexpert_model.model.cfg["heatmap_flag"] = False

        visualization = show_cam_on_image(img_np, grayscale_cam[0], use_rgb=True)
        PIL.Image.fromarray(visualization).save("./{}_Heatmap.jpg".format(file.replace(".jpg", '')))
    else:
        print("File is not an patient image!")
